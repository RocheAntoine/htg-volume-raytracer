//
// Created by antoine on 25/06/2020.
//

#include "HTGGPURaytracer.h"
#include "../utils/Ray_t.h"
#include "../utils/floatVec3.h"
#include <algorithm>

#ifndef OPENHTG_USE_CUDA
#include <omp.h>
#endif


OHTG_HOST void HTGGPURaytracer::setWindowSize(unsigned int widthIn, unsigned int heightIn)
{
	width = widthIn;
	height = heightIn;
#ifdef OPENHTG_USE_CUDA
	cudaMalloc(&colorBuffer, width * height * sizeof(char) * 4);
#else
	colorBuffer = (unsigned char *) malloc(width * height * sizeof(char) * 4);
#endif
}


OHTG_HOST void HTGGPURaytracer::getBuffer(unsigned char * & buffer) const
{
#ifdef OPENHTG_USE_CUDA
	cudaMemcpy(buffer, colorBuffer, sizeof(unsigned char) * width * height * 4, cudaMemcpyDeviceToHost);
#else
	std::copy(colorBuffer, colorBuffer + (width * height * 4), buffer);
#endif
}

OHTG_DEVICE inline unsigned int HTGGPURaytracer::xytoi(unsigned x, unsigned y) const
{
	return y * width + x;
}


OHTG_DEVICE void HTGGPURaytracer::run()
{
#ifdef OPENHTG_USE_CUDA
	unsigned i = threadIdx.x + blockIdx.x * blockDim.x;
	unsigned j = threadIdx.y + blockIdx.y * blockDim.y;

	if (i < width && j < height)
	{
#else
#pragma omp parallel for collapse(2) default(shared) schedule(dynamic)
	for (unsigned i = 0; i < width; ++i)
	{
		for (unsigned j = 0; j < height; ++j)
		{
#endif
			float dx = (float) i / (float) width * 2.f - 1.f;
			float dy = (float) j / (float) height * 2.f - 1.f;
			vec3 ray_origin = camera.eye;
			vec3 ray_direction = vec3(camera.U * dx + camera.V * dy + camera.W).normalize();

			if (fabs(ray_direction.x) < epsilon)
			{
				ray_direction.x = epsilon;
//			return;
			}

			if (fabs(ray_direction.y) < epsilon)
			{
				ray_direction.y = epsilon;
//			return;
			}

			if (fabs(ray_direction.z) < epsilon)
			{
				ray_direction.z = epsilon;
//			return;
			}

			Ray_t ray(ray_origin, ray_direction);

			vec3ToColorBuffer(trace(ray), xytoi(i, j));
#ifndef OPENHTG_USE_CUDA
		}
#endif
	}
}

OHTG_DEVICE void HTGGPURaytracer::vec3ToColorBuffer(const vec3 &vec, unsigned index) const
{
	colorBuffer[index * 4] = static_cast<unsigned char>(vec.x * 255.f);
	colorBuffer[index * 4 + 1] = static_cast<unsigned char>(vec.y * 255.f);
	colorBuffer[index * 4 + 2] = static_cast<unsigned char>(vec.z * 255.f);
}


template<>
OHTG_DEVICE void
GPUHyperTreeGrid::getNodeColor<HTG_RENDERING_MAX_LEVEL>(const vec3 &, payload_t &, unsigned) const
{}

OHTG_DEVICE vec3 HTGGPURaytracer::trace(const Ray_t &ray)
{
	vec3 minBox = htg->minBound;
	vec3 maxBox = htg->maxBound;

	vec3 invdir = vec3(1.f) / ray.direction;

	vec3 t0 = (minBox - ray.origin) * invdir;
	vec3 t1 = (maxBox - ray.origin) * invdir;
	vec3 near = vec3::min(t0, t1);
	vec3 far = vec3::max(t0, t1);
	float tmin = fmaxf(0.f, vec3::max(near)) + epsilon;
	float tmax = vec3::min(far) - epsilon;
	vec3 pos;


	payload_t payload;
	payload.maxLevel = maxLevel;

	unsigned count = 0;
	while (tmin < tmax)
	{
		pos = ray.pointAtDistance(tmin);
		unsigned index = htg->xyzToTreeIndex(pos);
		if (index >= htg->numberOfHyperTrees)
		{
			//			printf("%f, %f, %f, %f, %f\n", pos.x, pos.y, pos.z, tmin, tmax);
			break;
		}
		htg->getTreeBounds(index, minBox, maxBox);
		unsigned numberOfNodes = htg->numberofNodes[index];
		if (numberOfNodes)
		{
			unsigned long hyperTreeOffset = htg->hyperTreeOffset[index];
			unsigned long elderChildOffset = htg->elderChildOffset[index];
			if (!htg->isMasked(hyperTreeOffset))
			{
				payload.offset = hyperTreeOffset;
				payload.elderChildSize = htg->numberofNodes[index];
				payload.elderChildOffset = elderChildOffset;
				payload.direction = ray.direction * htg->dimensions;

				if (htg->elderChildIndex[elderChildOffset] != UINT32_MAX)
				{
					pos = (pos - minBox) / (maxBox - minBox);
					htg->getNodeColor<1u>(pos, payload, 0u);
				}
				else
				{
					vec3 tmp0 = (minBox - pos) * invdir;
					vec3 tmp1 = (maxBox - pos) * invdir;
					float opacity = fminf(vec3::min(vec3::max(tmp0, tmp1)) * htg->opacityFactor * htg->calculOpacity(payload.offset), 1.f);
					htg->calculColor(payload, 0, opacity);
				}
			}
			if (payload.ratio < 0.01f)
			{
				break;
			}
		}

		if (++count >= htg->numberOfHyperTrees)
		{
			break;
		}
		t0 = (minBox - ray.origin) * invdir;
		t1 = (maxBox - ray.origin) * invdir;

		far = vec3::max(t0, t1);

		tmin = vec3::min(far) + epsilon;

	}

	return payload.color * (1.f - payload.ratio) + background * payload.ratio;
}

OHTG_GLOBAL void kernelLauncher::launchRays(HTGGPURaytracer raytracer)
{
	raytracer.run();
}


OHTG_HOST void HTGGPURaytracer::render() const
{
	//	printf("Launch 2\n");

#ifdef OPENHTG_USE_CUDA
	dim3 dimBlock(16, 16);
	dim3 dimGrid(ceil((float)width / (float)dimBlock.x), ceil((float)height / (float)dimBlock.y));
	kernelLauncher::launchRays<<<dimGrid, dimBlock >> > (*this);
#else
	kernelLauncher::launchRays(*this);
#endif
}