//
// Created by antoine on 30/06/2020.
//

#ifndef HTGVOLUMERAYTRACER_GPUHYPERTREEGRID_H
#define HTGVOLUMERAYTRACER_GPUHYPERTREEGRID_H

#include "../utils/floatVec3.h"
#include "../utils/payload.h"
#include <cstdint>

const float epsilon = 0.00001f;
const unsigned int numberOfColor = 4096;


class GPUHyperTreeGrid
{
public:

	vec3 minBound;
	vec3 maxBound;
	vec3 dimensions;
	vec3 hyperTreeSize;
	unsigned int numberOfHyperTrees;

	float opacityFactor;
	float scalarOpacityMin;
	float scalarOpacityMax;

	float scalarColorMin;
	float scalarColorMax;

	bool volumeRendering;
	bool scalarContribution;
	bool secondScalarContribution;

	unsigned long int *hyperTreeOffset;
	unsigned int *elderChildIndex;
	unsigned int *lookUpTable;
	unsigned char *bitMask;
	unsigned int *numberofNodes;
	unsigned long *elderChildOffset;
	float *scalarOpacity;
	float *scalar;

	float *primaryTransferFunctionIndex;
	float *primaryTransferFunctionValue;
	float *secondaryTransferFunctionIndex;
	float *secondaryTransferFunctionValue;

	GPUHyperTreeGrid() = default;
	~GPUHyperTreeGrid() = default;


	OHTG_DEVICE inline bool isMasked(unsigned long int id) const
	{
//		return false;
		return (bitMask && bitMask[id / 8] & (0x80u >> (id % 8))) != 0;
	}

	OHTG_DEVICE static inline bool pointInBox(const vec3 &pos)
	{
		return pos.x >= 0.f && pos.x < 1.f && pos.y >= 0.f && pos.y < 1.f && pos.z >= 0.f && pos.z < 1.f;
	}

	OHTG_DEVICE static inline vec3 unsignedToVec3(unsigned int colorIn)
	{
		vec3 colorOut = vec3((float) ((colorIn & 0xFF0000u) >> 16u) / 255.f,
							 (float) ((colorIn & 0xFF00u) >> 8u) / 255.f,
							 (float) (colorIn & 0xFFu) / 255.f);
		return colorOut;
	}


	OHTG_DEVICE static void calculateChildUpdatePos(vec3 &pos, unsigned char &idChild)
	{
		vec3 posInNode;

		posInNode.x = fminf(floorf(pos.x * 2.f), 1.f);
		posInNode.y = fminf(floorf(pos.y * 2.f), 1.f);
		posInNode.z = fminf(floorf(pos.z * 2.f), 1.f);

		idChild = (unsigned char) posInNode.x;
		idChild += ((unsigned char) posInNode.y) << 1u;
		idChild += ((unsigned char) posInNode.z) << 2u;

		pos = pos * 2.f - posInNode;
	}

	OHTG_DEVICE inline float
	calculTransferFunction(float value, const float *transferFunctionIndex, const float *transferFunctionValue) const
	{
		unsigned index = 0;
		while (value > transferFunctionIndex[index + 1])
			++index;

		float minValue = transferFunctionIndex[index];
		float maxValue = transferFunctionIndex[index + 1];
		float weight = (value - minValue) / (maxValue - minValue);
//		printf("current value : %f; min value : %f, max value :  %f, weight : %f, index : %d \n", value, minValue, maxValue, weight, index);
		return transferFunctionValue[index] * (1.f - weight) + transferFunctionValue[index + 1] * weight;
	}

	OHTG_DEVICE inline float calculOpacity(unsigned long index) const
	{
		if (!volumeRendering)
			return 1.f;

		float firstOpacity = scalarContribution ? calculTransferFunction(
				scalar[index],
				primaryTransferFunctionIndex,
				primaryTransferFunctionValue) : 1.f;

		float secondOpacity = secondScalarContribution ? calculTransferFunction(
				scalarOpacity[index],
				secondaryTransferFunctionIndex,
				secondaryTransferFunctionValue) : 1.f;

		return firstOpacity * secondOpacity;
	}

	OHTG_DEVICE inline void calculColor(payload_t &payload, unsigned int index, float opacity) const
	{
		payload.color += unsignedToVec3(
				lookUpTable[(unsigned) ((float(numberOfColor) - 0.01f) *
										fmaxf(fminf((scalar[index + payload.offset] - scalarColorMin) /
												(scalarColorMax - scalarColorMin), 1.f), 0.f))]) * payload.ratio *
						 opacity;
		payload.ratio *= 1.f - opacity;
	}

	template<unsigned level>
	OHTG_DEVICE inline void
	processChild(payload_t &payload, vec3 &pos, float dist, unsigned elderChild, bool toStop) const
	{
		unsigned char idChild;

		calculateChildUpdatePos(pos, idChild);
		unsigned newIndex = elderChild + idChild;
		if (!isMasked(payload.offset + newIndex))
		{
			if (toStop || (newIndex >= payload.elderChildSize) ||
				(elderChildIndex[payload.elderChildOffset + newIndex] == UINT32_MAX))
			{
				float opacity = fminf(((dist + epsilon) / (float) (1u << level)) * opacityFactor *
									calculOpacity(payload.offset + newIndex), 1.f);
				calculColor(payload, newIndex, opacity);
			}
			else
			{
				getNodeColor<level + 1>(pos, payload, newIndex);
			}
		}
	}

	template<unsigned level>
	OHTG_DEVICE void
	getNodeColor(const vec3 &pos, payload_t &payload, unsigned int index) const
	{
		unsigned int elderChild = elderChildIndex[payload.elderChildOffset + index];
		vec3 dist{0.5f};
		float minDist;
		float midDist;
		float maxDist;
		vec3 newPos = pos;
//		unsigned newIndex;
//		unsigned char idChild{0};
//		float opacity;
		bool toStop = level == payload.maxLevel;

		vec3 t0 = (vec3(0.f) - pos) / payload.direction;
		vec3 t1 = (vec3(1.f) - pos) / payload.direction;
		float endDist = vec3::min(vec3::max(t0, t1));

		dist = (dist - pos) / payload.direction;
		if (dist.x < 0.f)
		{
			dist.x = 10.f;
		}
		if (dist.y < 0.f)
		{
			dist.y = 10.f;
		}
		if (dist.z < 0.f)
		{
			dist.z = 10.f;
		}

		minDist = vec3::min(dist);
		maxDist = vec3::max(dist);
		midDist = dist.x + dist.y + dist.z - maxDist - minDist;


		/// ADD process
		processChild<level>(payload, newPos, fminf(minDist, endDist), elderChild, toStop);

		newPos = pos + payload.direction * (minDist + epsilon);
		if (payload.ratio > 0.01f && minDist < 10.f && pointInBox(newPos))
		{
			processChild<level>(payload, newPos, fminf(midDist, endDist) - minDist, elderChild, toStop);
		}
////
		newPos = pos + payload.direction * (midDist + epsilon);
//		printf("%f, %f, %f\n", newPos.x, newPos.y, newPos.z);

		if (payload.ratio > 0.01f && midDist < 10.f && pointInBox(newPos))
		{
			processChild<level>(payload, newPos, fminf(maxDist, endDist) - midDist, elderChild, toStop);
		}

		newPos = pos + payload.direction * (maxDist + epsilon);
		if (payload.ratio > 0.01f && maxDist < 10.f && pointInBox(newPos))
		{
			processChild<level>(payload, newPos, endDist - maxDist, elderChild, toStop);
		}

	}


	OHTG_DEVICE inline unsigned xyzToTreeIndex(const vec3 &point) const
	{
		vec3 treePos = (point - minBound) / hyperTreeSize;
		unsigned index = unsigned(floorf(treePos.z) * dimensions.y * dimensions.x + floorf(treePos.y) * dimensions.x +
								  floorf(treePos.x));
		return index;
	}

	OHTG_HOST void init()
	{
		hyperTreeSize = (maxBound - minBound) / dimensions;
		numberOfHyperTrees = (unsigned) (dimensions.x * dimensions.y * dimensions.z);
	}

	OHTG_DEVICE inline void getTreeBounds(unsigned index, vec3 &minBound_, vec3 &maxBound_)
	{
		uint32_t XxY = (dimensions.x * dimensions.y);
		vec3 treePos;
		treePos.z = index / XxY;

		index -= (unsigned) (treePos.z * XxY);
		treePos.y = index / (unsigned) dimensions.x;
		treePos.x = index % (unsigned) dimensions.x;

		minBound_ = treePos * hyperTreeSize + minBound;
		maxBound_ = (treePos + 1) * hyperTreeSize + minBound;
	}
};


#endif //HTGVOLUMERAYTRACER_GPUHYPERTREEGRID_H
