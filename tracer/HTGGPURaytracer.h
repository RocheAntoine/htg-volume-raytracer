//
// Created by antoine on 25/06/2020.
//

#ifndef HTGVOLUMERAYTRACER_HTGGPURAYTRACER_H
#define HTGVOLUMERAYTRACER_HTGGPURAYTRACER_H

#include "../utils/floatVec3.h"
#include "GPUHyperTreeGrid.h"

//const float epsilon = 0.0001f;

class HTGGPURaytracer;

class Ray_t;

typedef struct camera
{
	vec3 eye;
	vec3 look_at;
	vec3 up;
	vec3 U;
	vec3 V;
	vec3 W;
	float ratio;
} camera_t;


namespace kernelLauncher
{
	OHTG_GLOBAL void launchRays(HTGGPURaytracer raytracer);
}

class HTGGPURaytracer
{
public:
	unsigned char *colorBuffer;
	unsigned int width;
	unsigned int height;

	vec3 background;
	unsigned int maxLevel;

	GPUHyperTreeGrid *htg;

	camera_t camera;

	OHTG_DEVICE inline unsigned int xytoi(unsigned x, unsigned y) const;

public:
	HTGGPURaytracer() : width(0), height(0), colorBuffer(nullptr), htg(nullptr), background(0.3f){}

	~HTGGPURaytracer() = default;

	OHTG_HOST void setWindowSize(unsigned int widthIn, unsigned int heightIn);

	OHTG_HOST void getBuffer(unsigned char * & buffer) const;

	OHTG_DEVICE void run();

	OHTG_HOST void render() const;

	OHTG_DEVICE vec3 trace(const Ray_t &ray);

	OHTG_DEVICE void vec3ToColorBuffer(const vec3 &vec, unsigned index) const;
};


#endif //HTGVOLUMERAYTRACER_HTGGPURAYTRACER_H
