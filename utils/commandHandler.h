//
// Created by rochea on 15/07/2020.
//

#ifndef HTGVOLUMERAYTRACER_COMMANDHANDLER_H
#define HTGVOLUMERAYTRACER_COMMANDHANDLER_H

#include <iostream>
#include <fstream>

namespace commandHandler
{
	template<class streamType>
	void getCommand(streamType & stream);
	void getCommandScalar(std::stringstream&);
	void getCommandCamera(std::stringstream&);
	void getCommandScreenShot(std::stringstream&);
	void getCommandTransfer(std::stringstream&);
	void displayHelp();

	unsigned nbScreenShots = 0;
};
#include "commandHandler.txx"

#endif //HTGVOLUMERAYTRACER_COMMANDHANDLER_H
