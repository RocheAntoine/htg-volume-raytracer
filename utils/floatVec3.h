//
// Created by antoine on 29/06/2020.
//

#ifndef HTGVOLUMERAYTRACER_FLOATVEC3_H
#define HTGVOLUMERAYTRACER_FLOATVEC3_H

#include <cmath>
#include "cudaDefinitions.h"

class vec3
{
public:
	float x;
	float y;
	float z;

	OHTG_HOST_DEVICE vec3() : x(0.f), y(0.f), z(0.f){}
	OHTG_HOST_DEVICE vec3(float a, float b, float c) : x(a), y(b), z(c){}
	OHTG_HOST_DEVICE vec3(float a) : x(a), y(a), z(a){}
	OHTG_HOST_DEVICE vec3(const vec3 & vec) : x(vec.x), y(vec.y), z(vec.z){}

	OHTG_HOST_DEVICE vec3 operator+(const vec3 &vec) const
	{
		return vec3(x + vec.x, y + vec.y, z + vec.z);
	}

	OHTG_HOST_DEVICE vec3 operator*(const vec3 &vec) const
	{
		return vec3(x * vec.x, y * vec.y, z * vec.z);
	}

	OHTG_HOST_DEVICE vec3 operator/(const vec3 &vec) const
	{
		return vec3(x / vec.x, y / vec.y, z / vec.z);
	}

	OHTG_HOST_DEVICE vec3 operator-(const vec3 &vec) const
	{
		return vec3(x - vec.x, y - vec.y, z - vec.z);
	}

	OHTG_HOST_DEVICE vec3 operator+(float a) const
	{
		return vec3(x + a, y + a, z + a);
	}

	OHTG_HOST_DEVICE vec3 operator*(float a) const
	{
		return vec3(x * a, y * a, z * a);
	}

	OHTG_HOST_DEVICE vec3 operator/(float a) const
	{
		return vec3(x / a, y / a, z / a);
	}

	OHTG_HOST_DEVICE vec3 operator-(float a) const
	{
		return vec3(x - a, y - a, z - a);
	}


	OHTG_HOST_DEVICE vec3& operator*=(const vec3& vec)
	{
		*this = *this * vec;
		return *this;
	}

	OHTG_HOST_DEVICE vec3& operator-=(const vec3& vec)
	{
		*this = *this - vec;
		return *this;
	}

	OHTG_HOST_DEVICE vec3& operator+=(const vec3& vec)
	{
		*this = *this + vec;
		return *this;
	}

	OHTG_HOST_DEVICE vec3& operator*=(float a)
	{
		*this = *this * a;
		return *this;
	}

	OHTG_HOST_DEVICE vec3& operator/=(float a)
	{
		*this = *this / a;
		return *this;
	}

	OHTG_HOST_DEVICE float dot(const vec3 &b) const
	{
		return x * b.x + y * b.y + z * b.z;
	}

	OHTG_HOST_DEVICE vec3 cross( const vec3 &b) const
	{
		return vec3(y * b.z - z * b.y, z * b.x - x * b.z, x * b.y - y * b.x);
	}

	OHTG_HOST_DEVICE vec3 normalize() const
	{
		return *this / sqrtf(dot(*this));
	}

	OHTG_HOST_DEVICE float length() const
	{
		return sqrtf(dot(*this));
	}

	OHTG_HOST_DEVICE vec3 abs() const
	{
		return vec3(std::abs(x), std::abs(y), std::abs(z));
	}

	OHTG_HOST_DEVICE static vec3 min(const vec3& a, const vec3&b)
	{
		return vec3(std::fminf(a.x, b.x), std::fminf(a.y, b.y), std::fminf(a.z, b.z));
	}

	OHTG_HOST_DEVICE static vec3 max(const vec3& a, const vec3&b)
	{
		return vec3(std::fmaxf(a.x, b.x), std::fmaxf(a.y, b.y), std::fmaxf(a.z, b.z));
	}

	OHTG_HOST_DEVICE static float min(const vec3& vec)
	{
		return std::fminf(vec.x, std::fminf(vec.y, vec.z));
	}

	OHTG_HOST_DEVICE static float max(const vec3& vec)
	{
		return std::fmaxf(vec.x, std::fmaxf(vec.y, vec.z));
	}

};




#endif //HTGVOLUMERAYTRACER_FLOATVEC3_H
