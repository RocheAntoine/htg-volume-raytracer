// Copyright 2019-2020 Intel Corporation
// SPDX-License-Identifier: Apache-2.0

#pragma once

#include <functional>
#include <string>
#include <vector>
#include "floatVec3.h"
#ifdef USE_INTERACTIVE_RENDERING_OPENGL
#include <GLFW/glfw3.h>
#endif

class TransferFunctionWidget
{
public:
	TransferFunctionWidget(std::vector<float> &opacityIndex,
						   std::vector<float> &opacityValue,
						   std::vector<float> &colorIndex,
						   std::vector<vec3> &colorValue,
						   float minIndex,
						   float maxIndex,
						   bool isColored,
						   const std::string &widgetName = "Transfer Function");

	~TransferFunctionWidget();

	// update UI and process any UI events
#ifdef USE_INTERACTIVE_RENDERING_OPENGL
	void updateUI();
#endif
	bool tfnChanged;

	void getSampledColorsAndOpacities(
			int numSamples,
			std::vector<float>& opacityOut,
			std::vector<vec3>& colorOut);

private:
	void loadDefaultMaps();

	void loadFromFile(const char* filename);

	void saveInFile(const char* filename);

	vec3 interpolateColor(float x);

	float interpolateOpacity(float x);
#ifdef USE_INTERACTIVE_RENDERING_OPENGL
	void updateTfnPaletteTexture();

	void drawEditor();
	GLuint tfnPaletteTexture{0};
#endif
	// all available transfer functions
	std::vector<float> *opacityIndex;
	std::vector<float> *opacityValue;

	std::vector<float> *colorIndex;
	std::vector<vec3> *colorValue;

	float minIndex, maxIndex;
	bool isColored;
	char filename[128];

	// texture for displaying transfer function color palette


	// widget name (use different names to support multiple concurrent widgets)
	std::string widgetName;
};
