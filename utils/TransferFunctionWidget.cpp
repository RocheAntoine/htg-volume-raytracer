// Copyright 2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0

#include "TransferFunctionWidget.h"
#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <fstream>

#ifdef USE_INTERACTIVE_RENDERING_OPENGL

#include "imgui/imgui.h"

#endif
namespace help
{

	template<typename T>
	int find_idx(const std::vector<T> &A, float p)
	{
		auto found = std::upper_bound(
				A.begin(), A.end(), T(p), [](const T &a, const T &b)
				{ return a < b; });
		return std::distance(A.begin(), found);
	}

	template<typename T>
	T clamp(T value, T min, T max)
	{
		assert(!(max < min));
		return (value < min) ? min : (max < value) ? max : value;
	}

	float lerp(const float &l,
	           const float &r,
	           const float &pl,
	           const float &pr,
	           const float &p)
	{
		const float dl = std::abs(pr - pl) > 0.0001f ? (p - pl) / (pr - pl) : 0.f;
		const float dr = 1.f - dl;
		return l * dr + r * dl;
	}

}  // namespace help

TransferFunctionWidget::TransferFunctionWidget(std::vector<float> &opacityIndex,
                                               std::vector<float> &opacityValue,
                                               std::vector<float> &colorIndex,
                                               std::vector<vec3> &colorValue,
                                               float indexMin,
                                               float indexMax,
                                               bool isColored,
                                               const std::string &_widgetName) :
		opacityIndex(&opacityIndex),
		opacityValue(&opacityValue),
		colorIndex(&colorIndex),
		colorValue(&colorValue),
		minIndex(indexMin),
		maxIndex(indexMax),
		widgetName(_widgetName),
		isColored(isColored)
{
	if (isColored)
	{
		loadDefaultMaps();
		tfnChanged = true;
	}
	// set ImGui double click time to 1s, so it also works for slower frame rates

}

TransferFunctionWidget::~TransferFunctionWidget()
{
#ifdef USE_INTERACTIVE_RENDERING_OPENGL
	if (tfnPaletteTexture)
	{
//		glDeleteTextures(1, &tfnPaletteTexture);
	}
#endif
}

#ifdef USE_INTERACTIVE_RENDERING_OPENGL

void TransferFunctionWidget::updateUI()
{
	if (tfnChanged)
	{
		if(isColored)
		{
			updateTfnPaletteTexture();
		}
		tfnChanged = false;
	}

	// need a unique ImGui group name per widget
	if (!ImGui::Begin(widgetName.c_str()))
	{
		ImGui::End();
		return;
	}

	ImGui::Text("Linear Transfer Function");

	ImGui::Separator();
	ImGui::Separator();

	drawEditor();

	ImGui::Text("File name");
	ImGui::InputText("", filename, 128);
	if (ImGui::Button("Save"))
	{
		saveInFile(filename);
	}
	ImGui::SameLine();
	if (ImGui::Button("Load"))
	{
		loadFromFile(filename);
		tfnChanged = true;
	}

	ImGui::End();
}

void TransferFunctionWidget::loadFromFile(const char *filename)
{
	std::ifstream f(filename);
	int size;
	f >> size;
	opacityIndex->clear();
	opacityValue->clear();

	opacityIndex->resize(size);
	opacityValue->resize(size);

	for(int i = 0; i < size; ++i)
	{
		f >> (*opacityIndex)[i];
	}

	for(int i = 0; i < size; ++i)
	{
		f >> (*opacityValue)[i];
	}

	f >> size;
	colorIndex->clear();
	colorValue->clear();

	colorIndex->resize(size);
	colorValue->resize(size);

	for(int i = 0; i < size; ++i)
	{
		f >> (*colorIndex)[i];
	}

	for(int i = 0; i < size; ++i)
	{
		vec3 tmp;
		f >> tmp.x;
		f >> tmp.y;
		f >> tmp.z;
		(*colorValue)[i] = tmp;
	}

}

void TransferFunctionWidget::saveInFile(const char *filename)
{
	std::ofstream of(filename);
	of << opacityIndex->size() << ' ';
	for (float n : *opacityIndex)
		of << n << ' ';
	for (float n : *opacityValue)
		of << n << ' ';

	of << colorIndex->size() << ' ';
	for (float n : *colorIndex)
		of << n << ' ';
	for (const vec3 &n : *colorValue)
	{
		of << n.x << ' ';
		of << n.y << ' ';
		of << n.z << ' ';
	}
}

void TransferFunctionWidget::updateTfnPaletteTexture()
{
	const size_t textureWidth = 256, textureHeight = 1;

	// backup currently bound texture
	GLint prevBinding = 0;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevBinding);

	// create transfer function palette texture if it doesn't exist
	if (!tfnPaletteTexture)
	{
		glGenTextures(1, &tfnPaletteTexture);
		glBindTexture(GL_TEXTURE_2D, tfnPaletteTexture);
		glTexImage2D(GL_TEXTURE_2D,
		             0,
		             GL_RGBA8,
		             textureWidth,
		             textureHeight,
		             0,
		             GL_RGBA,
		             GL_UNSIGNED_BYTE,
		             0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	// sample the palette then upload the data
	std::vector<float> paletteOpacity;
	std::vector<vec3> paletteColor;
	getSampledColorsAndOpacities(textureWidth, paletteOpacity, paletteColor);

	std::vector<float> palette;

	for (uint32_t i = 0; i < paletteOpacity.size(); ++i)
	{
		palette.push_back(paletteColor[i].x);
		palette.push_back(paletteColor[i].y);
		palette.push_back(paletteColor[i].z);
		palette.push_back(paletteOpacity[i]);
	}

	// save palette to texture
	glBindTexture(GL_TEXTURE_2D, tfnPaletteTexture);
	glTexImage2D(GL_TEXTURE_2D,
	             0,
	             GL_RGB,
	             textureWidth,
	             textureHeight,
	             0,
	             GL_RGBA,
	             GL_FLOAT,
	             static_cast<const void *>(palette.data()));

	// restore previously bound texture
	if (prevBinding)
	{
		glBindTexture(GL_TEXTURE_2D, prevBinding);
	}
}

void TransferFunctionWidget::drawEditor()
{
	// only God and me know what do they do ...
	ImDrawList *draw_list = ImGui::GetWindowDrawList();
	float canvas_x = ImGui::GetCursorScreenPos().x;
	float canvas_y = ImGui::GetCursorScreenPos().y;
	float canvas_avail_x = ImGui::GetContentRegionAvail().x;
	float canvas_avail_y = ImGui::GetContentRegionAvail().y;
	const float mouse_x = ImGui::GetMousePos().x;
	const float mouse_y = ImGui::GetMousePos().y;
	const float scroll_x = ImGui::GetScrollX();
	const float scroll_y = ImGui::GetScrollY();
	const float margin = 10.f;
	const float width = canvas_avail_x - 2.f * margin;
	const float height = 260.f;
	const float color_len = 9.f;
	const float opacity_len = 7.f;
	const float xFactor = 1.f / (maxIndex - minIndex);

	// draw preview texture
	ImGui::SetCursorScreenPos(ImVec2(canvas_x + margin, canvas_y));
	ImGui::Image(reinterpret_cast<void *>(tfnPaletteTexture),
	             ImVec2(width, height));
	if (ImGui::IsItemHovered())
	{
		ImGui::SetTooltip("Double left click to add new control point");
	}

	ImGui::SetCursorScreenPos(ImVec2(canvas_x, canvas_y));
	for (int i = 0; i < opacityIndex->size() - 1; ++i)
	{
		std::vector<ImVec2> polyline;
		float pos = ((*opacityIndex)[i] - minIndex) * xFactor;
		polyline.emplace_back(canvas_x + margin + pos * width,
		                      canvas_y); // canvas_y + height
		polyline.emplace_back(
				canvas_x + margin + pos * width,
				canvas_y + height - (*opacityValue)[i] * height);

		pos = ((*opacityIndex)[i + 1] - minIndex) * xFactor;

		polyline.emplace_back(
				canvas_x + margin + pos * width + 1,
				canvas_y + height - (*opacityValue)[i + 1] * height);
		polyline.emplace_back(
				canvas_x + margin + pos * width + 1,
				canvas_y); // canvas_y + height
		draw_list->AddConvexPolyFilled(
				polyline.data(), polyline.size(), 0xFFD8D8D8);
	}
	canvas_y += height + margin;
	canvas_avail_y -= height + margin;

	if (isColored)
	{
		// draw color control points
		ImGui::SetCursorScreenPos(ImVec2(canvas_x, canvas_y));

		// draw circle background
		draw_list->AddRectFilled(
				ImVec2(canvas_x + margin, canvas_y - margin),
				ImVec2(canvas_x + margin + width, canvas_y - margin + 2.5 * color_len),
				0xFF474646);

		// draw circles
		for (int i = colorIndex->size() - 1; i >= 0; --i)
		{
			const ImVec2 pos(canvas_x + width * ((*colorIndex)[i] - minIndex) * xFactor + margin,
			                 canvas_y);
			ImGui::SetCursorScreenPos(ImVec2(canvas_x, canvas_y));

			// white background
			draw_list->AddTriangleFilled(ImVec2(pos.x - 0.5f * color_len, pos.y),
			                             ImVec2(pos.x + 0.5f * color_len, pos.y),
			                             ImVec2(pos.x, pos.y - color_len),
			                             0xFFD8D8D8);
			draw_list->AddCircleFilled(
					ImVec2(pos.x, pos.y + 0.5f * color_len), color_len, 0xFFD8D8D8);

			// draw picker
			ImVec4 picked_color = ImColor((*colorValue)[i].x,
			                              (*colorValue)[i].y,
			                              (*colorValue)[i].z,
			                              1.f);
			ImGui::SetCursorScreenPos(
					ImVec2(pos.x - color_len, pos.y + 1.5f * color_len));
			if (ImGui::ColorEdit4(("##ColorPicker" + std::to_string(i)).c_str(),
			                      (float *) &picked_color,
			                      ImGuiColorEditFlags_NoAlpha |
			                      ImGuiColorEditFlags_NoInputs |
			                      ImGuiColorEditFlags_NoLabel |
			                      ImGuiColorEditFlags_AlphaPreview |
			                      ImGuiColorEditFlags_NoOptions |
			                      ImGuiColorEditFlags_NoTooltip))
			{
				(*colorValue)[i].x = picked_color.x;
				(*colorValue)[i].y = picked_color.y;
				(*colorValue)[i].z = picked_color.z;
				tfnChanged = true;
			}
			if (ImGui::IsItemHovered())
			{
				// convert float color to char
				int cr = static_cast<int>(picked_color.x * 255);
				int cg = static_cast<int>(picked_color.y * 255);
				int cb = static_cast<int>(picked_color.z * 255);

				// setup tooltip
				ImGui::BeginTooltip();
				ImVec2 sz(
						ImGui::GetFontSize() * 4 + ImGui::GetStyle().FramePadding.y * 2,
						ImGui::GetFontSize() * 4 + ImGui::GetStyle().FramePadding.y * 2);
				ImGui::ColorButton(
						"##PreviewColor",
						picked_color,
						ImGuiColorEditFlags_NoAlpha | ImGuiColorEditFlags_AlphaPreview,
						sz);
				ImGui::SameLine();
				ImGui::Text(
						"Left click to edit\n"
						"HEX: #%02X%02X%02X\n"
						"RGB: [%3d,%3d,%3d]\n(%.2f, %.2f, %.2f)",
						cr,
						cg,
						cb,
						cr,
						cg,
						cb,
						picked_color.x,
						picked_color.y,
						picked_color.z);
				ImGui::EndTooltip();
			}
		}

		for (int i = 0; i < colorIndex->size(); ++i)
		{
			const ImVec2 pos(canvas_x + width * ((*colorIndex)[i] - minIndex) * xFactor + margin,
			                 canvas_y);

			// draw button
			ImGui::SetCursorScreenPos(
					ImVec2(pos.x - color_len, pos.y - 0.5 * color_len));
			ImGui::InvisibleButton(("##ColorControl-" + std::to_string(i)).c_str(),
			                       ImVec2(2.f * color_len, 2.f * color_len));

			// dark highlight
			ImGui::SetCursorScreenPos(ImVec2(pos.x - color_len, pos.y));
			draw_list->AddCircleFilled(
					ImVec2(pos.x, pos.y + 0.5f * color_len),
					0.5f * color_len,
					ImGui::IsItemHovered() ? 0xFF051C33 : 0xFFBCBCBC);

			// delete color point
			if (ImGui::IsMouseDoubleClicked(1) && ImGui::IsItemHovered())
			{
				if (i > 0 && i < colorIndex->size() - 1)
				{
					colorIndex->erase(colorIndex->begin() + i);
					colorValue->erase(colorValue->begin() + i);
					tfnChanged = true;
				}
			}

				// drag color control point
			else if (ImGui::IsItemActive())
			{
				ImVec2 delta = ImGui::GetIO().MouseDelta;
				if (i > 0 && i < colorIndex->size() - 1)
				{
					(*colorIndex)[i] += delta.x / (width * xFactor);
					(*colorIndex)[i] = help::clamp((*colorIndex)[i],
					                               (*colorIndex)[i - 1],
					                               (*colorIndex)[i + 1]);

				}

				tfnChanged = true;
			}
		}
	}

	// draw opacity control points
	ImGui::SetCursorScreenPos(ImVec2(canvas_x, canvas_y));
	{
		// draw circles
		for (int i = 0; i < opacityIndex->size(); ++i)
		{
			const ImVec2 pos(canvas_x + width * ((*opacityIndex)[i] - minIndex) * xFactor + margin,
			                 canvas_y - height * (*opacityValue)[i] - margin);
			ImGui::SetCursorScreenPos(
					ImVec2(pos.x - opacity_len, pos.y - opacity_len));
			ImGui::InvisibleButton(("##OpacityControl-" + std::to_string(i)).c_str(),
			                       ImVec2(2.f * opacity_len, 2.f * opacity_len));
			ImGui::SetCursorScreenPos(ImVec2(canvas_x, canvas_y));

			// dark bounding box
			draw_list->AddCircleFilled(pos, opacity_len, 0xFF565656);

			// white background
			draw_list->AddCircleFilled(pos, 0.8f * opacity_len, 0xFFD8D8D8);

			// highlight
			draw_list->AddCircleFilled(
					pos,
					0.6f * opacity_len,
					ImGui::IsItemHovered() ? 0xFF051c33 : 0xFFD8D8D8);

			// setup interaction

			// delete opacity point
			if (ImGui::IsMouseDoubleClicked(1) && ImGui::IsItemHovered())
			{
				if (i > 0 && i < opacityIndex->size() - 1)
				{
					opacityIndex->erase(opacityIndex->begin() + i);
					opacityValue->erase(opacityValue->begin() + i);
					tfnChanged = true;
				}
			}
			else if (ImGui::IsItemActive())
			{
				ImVec2 delta = ImGui::GetIO().MouseDelta;
				(*opacityValue)[i] -= delta.y / height;
				(*opacityValue)[i] = help::clamp((*opacityValue)[i], 0.0f, 1.0f);
				if (i > 0 && i < opacityIndex->size() - 1)
				{
					(*opacityIndex)[i] += delta.x / (width * xFactor);
					(*opacityIndex)[i] = (*opacityIndex)[i] = help::clamp((*opacityIndex)[i],
					                                                      (*opacityIndex)[i - 1] + FLT_EPSILON,
					                                                      (*opacityIndex)[i + 1] - FLT_EPSILON);
				}
				tfnChanged = true;
			}
			else if (ImGui::IsItemHovered())
			{
				ImGui::SetTooltip(
						"Double right click button to delete point\n"
						"Left click and drag to move point");
			}
		}
	}

	// draw background interaction
	ImGui::SetCursorScreenPos(ImVec2(canvas_x + margin, canvas_y - margin));
	ImGui::InvisibleButton("##tfn_palette_color", ImVec2(width, 2.5 * color_len));

	// add color point
	if (ImGui::IsMouseDoubleClicked(0) && ImGui::IsItemHovered())
	{
		const float p = help::clamp(
				(mouse_x - canvas_x - margin - scroll_x) / (float) width, 0.f, 1.f) / xFactor + minIndex;
		const int ir = help::find_idx(*colorIndex, p);
		const int il = ir - 1;
		const float pr = (*colorIndex)[ir];
		const float pl = (*colorIndex)[il];
		const float r =
				help::lerp((*colorValue)[il].x, (*colorValue)[ir].x, pl, pr, p);
		const float g =
				help::lerp((*colorValue)[il].y, (*colorValue)[ir].y, pl, pr, p);
		const float b =
				help::lerp((*colorValue)[il].z, (*colorValue)[ir].z, pl, pr, p);

		colorIndex->insert(colorIndex->begin() + ir, p);
		colorValue->insert(colorValue->begin() + ir, vec3(r, g, b));
		tfnChanged = true;
	}

	if (ImGui::IsItemHovered())
	{
		ImGui::SetTooltip("Double left click to add new color point");
	}

	// draw background interaction
	ImGui::SetCursorScreenPos(
			ImVec2(canvas_x + margin, canvas_y - height - margin));
	ImGui::InvisibleButton("##tfn_palette_opacity", ImVec2(width, height));

	// add opacity point
	if (ImGui::IsMouseDoubleClicked(0) && ImGui::IsItemHovered())
	{
		const float x = help::clamp(
				(mouse_x - canvas_x - margin - scroll_x) / (float) width, 0.f, 1.f) / xFactor + minIndex;
		const float y = help::clamp(
				-(mouse_y - canvas_y + margin - scroll_y) / (float) height, 0.f, 1.f);
		const int idx = help::find_idx(*opacityIndex, x);

		opacityIndex->insert(opacityIndex->begin() + idx, x);
		opacityValue->insert(opacityValue->begin() + idx, y);
		tfnChanged = true;
	}

	// update cursors
	canvas_y += 4.f * color_len + margin;
	canvas_avail_y -= 4.f * color_len + margin;

	ImGui::SetCursorScreenPos(ImVec2(canvas_x, canvas_y));
}

#endif

void TransferFunctionWidget::getSampledColorsAndOpacities(
		int numSamples,
		std::vector<float> &opacityOut,
		std::vector<vec3> &colorValueOut)
{
	const float dx = (maxIndex - minIndex) / (numSamples - 1);

	for (int i = 0; i < numSamples; i++)
	{
		opacityOut.push_back(interpolateOpacity(i * dx + minIndex));
		colorValueOut.push_back(interpolateColor(i * dx + minIndex));
	}

}

void TransferFunctionWidget::loadDefaultMaps()
{
	colorIndex->emplace_back(minIndex);
	colorValue->emplace_back(0.f, 0.f, 1.f);

	colorIndex->emplace_back((minIndex + maxIndex) * 0.5f);
	colorValue->emplace_back(0.f, 1.f, 0.f);

	colorIndex->emplace_back(maxIndex);
	colorValue->emplace_back(1.f, 0.f, 0.f);
}

vec3 TransferFunctionWidget::interpolateColor(float x)
{
	auto firstIndex = colorIndex->front();
	auto firstColor = colorValue->front();
	if (x <= firstIndex)
	{
		return vec3(firstColor.x, firstColor.y, firstColor.z);
	}

	for (uint32_t i = 1; i < colorIndex->size(); i++)
	{
		auto currentIndex = (*colorIndex)[i];
		auto currentColor = (*colorValue)[i];

		auto previousIndex = (*colorIndex)[i - 1];
		auto previousColor = (*colorValue)[i - 1];

		if (x <= currentIndex)
		{
			const float t = (x - previousIndex) / (currentIndex - previousIndex);
			return vec3(previousColor.x, previousColor.y, previousColor.z) * (1.f - t) +
			       vec3(currentColor.x, currentColor.y, currentColor.z) * t;
		}
	}

	auto last = colorValue->back();
	return vec3(last.x, last.y, last.z);
}

float TransferFunctionWidget::interpolateOpacity(float x)
{
	auto firstIndex = opacityIndex->front();
	auto firstValue = opacityValue->front();
	if (x <= firstIndex)
	{
		return firstValue;
	}

	for (uint32_t i = 1; i < opacityIndex->size(); i++)
	{
		auto currentIndex = (*opacityIndex)[i];
		auto currentValue = (*opacityValue)[i];

		auto previousIndex = (*opacityIndex)[i - 1];
		auto previousValue = (*opacityValue)[i - 1];

		if (x <= currentIndex)
		{
			const float t = (x - previousIndex) / (currentIndex - previousIndex);
			return (1.f - t) * previousValue + t * currentValue;
		}
	}

	auto last = opacityValue->back();
	return last;
}

