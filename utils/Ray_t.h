//
// Created by antoine on 29/06/2020.
//

#ifndef HTGVOLUMERAYTRACER_RAY_T_H
#define HTGVOLUMERAYTRACER_RAY_T_H

#include "floatVec3.h"

class Ray_t
{
public:
	vec3 origin;
	vec3 direction;

	OHTG_DEVICE vec3 pointAtDistance(float t) const {return origin + direction * t;}
	OHTG_DEVICE Ray_t(const vec3& o, const vec3& d) : origin(o), direction(d){}
};


#endif //HTGVOLUMERAYTRACER_RAY_T_H
