//
// Created by antoine on 01/09/2020.
//

#ifndef HTGVOLUMERAYTRACER_HTGDATAGETTER_H
#define HTGVOLUMERAYTRACER_HTGDATAGETTER_H
#include <vector>
#include <string>

class HTGDataGetter
{
private:
	void* HTG;
	bool fromVTK;

public:
	void setHTG(void* HTG, bool fromVTK);

	void getDimensions(unsigned int dim[3]);
	std::vector<std::string> getScalarNames();
	void getScalar(const char *scalarName, std::vector<float> & vec);
	std::vector<unsigned char> getBitMask();
	std::vector<std::vector<unsigned int>> getElderChildIndex();
	std::vector<unsigned int> getNumberofTreeNodes();
	void getBounds(double boundsOut [6]);
	unsigned int getMaxNumberOfTrees();
	unsigned int getNumberOfLevels();

};


#endif //HTGVOLUMERAYTRACER_HTGDATAGETTER_H
