//
// Created by antoine on 01/07/2020.
//

#ifndef HTGVOLUMERAYTRACER_PAYLOAD_H
#define HTGVOLUMERAYTRACER_PAYLOAD_H
#include "floatVec3.h"
typedef struct payload
{
	vec3 color{0.f};
	vec3 direction{0.f};
	unsigned long offset{0};
	unsigned elderChildSize{0};
	unsigned long elderChildOffset{0};
	float ratio{1.f};
	unsigned int maxLevel{99u};
} payload_t;


#endif //HTGVOLUMERAYTRACER_PAYLOAD_H
