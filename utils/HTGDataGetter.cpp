//
// Created by antoine on 01/09/2020.
//

#include "HTGDataGetter.h"

#ifdef USE_VTK

#include "vtkHyperTreeGrid.h"
#include "vtkHyperTree.h"
#include "vtkCellData.h"
#include "vtkDataArray.h"
#include "vtkDoubleArray.h"
#include "vtkFloatArray.h"
#include "vtkUnsignedIntArray.h"
#include "vtkIntArray.h"
#include "vtkIdTypeArray.h"
#include "vtkDataArrayRange.h"
#include "vtkBitArray.h"

#include"vtkHyperTreeGridNonOrientedCursor.h"

#include "vtkHyperTreeGridEvaluateCoarse.h"

#endif

#ifdef USE_OPENHTG

#include "openhtg/openHyperTreeGrid.h"
#include "openhtg/openHyperTreeGridCursor.h"

#endif

#include <iostream>

#ifdef USE_VTK

bool checkCoordDistances(vtkDataArray *array)
{
	double dist = array->GetTuple1(1) - array->GetTuple1(0);

	for (int i = 2; i < array->GetNumberOfValues(); ++i)
	{
		double n = array->GetTuple1(i) - array->GetTuple1(i - 1);
		double diff = (dist - n) / dist;
		if (diff > 0.0001)
		{
			return false;
		}
	}
	return true;
}

#endif

void HTGDataGetter::setHTG(void *HTGIn, bool fromVTKIn)
{
	fromVTK = fromVTKIn;
	HTG = (void *) HTGIn;
	if (fromVTK)
	{
#ifdef USE_VTK

		auto tmpHTG = (vtkHyperTreeGrid *) HTG;

		if (tmpHTG->GetBranchFactor() != 2)
		{
			std::cerr << "Error : only 2 branch factor is supported" << std::endl;
			exit(EXIT_FAILURE);
		}

		if (tmpHTG->GetDimension() != 3)
		{
			std::cerr << "Error : only 3D is supported" << std::endl;
			exit(EXIT_FAILURE);
		}

		if (!checkCoordDistances(tmpHTG->GetXCoordinates()) ||
		    !checkCoordDistances(tmpHTG->GetYCoordinates()) ||
		    !checkCoordDistances(tmpHTG->GetZCoordinates()))
		{
			std::cerr << "Error : only uniform HTG supported" << std::endl;
			exit(EXIT_FAILURE);
		}

#else

		std::cerr << "Error : VTK not linked to the program" << std::endl;
		exit(EXIT_FAILURE);

#endif
	}
#ifndef USE_OPENHTG
	if(!fromVTK)
	{
		{
			std::cerr << "Error : OpenHTG not linked to the program" << std::endl;
			exit(EXIT_FAILURE);
		}
	}
#endif
}

void HTGDataGetter::getScalar(const char *scalarName, std::vector<float> &vec)
{
	if (fromVTK)
	{
#ifdef USE_VTK
		auto tmpHTG = (vtkHyperTreeGrid *) HTG;
		auto scalar = tmpHTG->GetCellData()->GetArray(scalarName);
		void *tab = vtkDoubleArray::SafeDownCast(scalar);

		if (tab)
		{
			vec = std::vector<float>(((vtkDoubleArray *) tab)->GetPointer(0),
			                         ((vtkDoubleArray *) tab)->GetPointer(scalar->GetNumberOfValues()));
			return;
		}

		tab = vtkFloatArray::SafeDownCast(scalar);
		if (tab)
		{
			vec = std::vector<float>(((vtkFloatArray *) tab)->GetPointer(0),
			                         ((vtkFloatArray *) tab)->GetPointer(scalar->GetNumberOfValues()));
			return;
		}

		tab = vtkUnsignedIntArray::SafeDownCast(scalar);
		if (tab)
		{
			vec = std::vector<float>(((vtkUnsignedIntArray *) tab)->GetPointer(0),
			                         ((vtkUnsignedIntArray *) tab)->GetPointer(scalar->GetNumberOfValues()));
			return;
		}

		tab = vtkIdTypeArray::SafeDownCast(scalar);
		if (tab)
		{
			vec = std::vector<float>((vtkIdType *) ((vtkIdTypeArray *) tab)->GetPointer(0),
			                         (vtkIdType *) ((vtkIdTypeArray *) tab)->GetPointer(scalar->GetNumberOfValues()));
			return;
		}

		std::cout << scalar->GetDataTypeAsString() << " not supported " << std::endl;
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		std::vector<float> &scalar = ((openHyperTreeGrid *) HTG)->getscalarArray(scalarName);
		vec.assign(scalar.begin(), scalar.end());
#endif
	}
}

std::vector<unsigned char> HTGDataGetter::getBitMask()
{
	if (fromVTK)
	{
#ifdef USE_VTK
		auto tmpHTG = (vtkHyperTreeGrid *) HTG;
		auto mask = tmpHTG->GetMask();
		if (!mask)
		{
			return std::vector<unsigned char>{};
		}
		std::vector<unsigned char> bitmask((unsigned char *) mask->GetPointer(0),
		                                   (unsigned char *) (mask->GetPointer(mask->GetNumberOfValues())));
		return bitmask;
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		auto bit = ((openHyperTreeGrid *) HTG)->getBitMask();
		return ((openHyperTreeGrid *) HTG)->getBitMask();
#endif
	}

}

std::vector<std::vector<unsigned int>> HTGDataGetter::getElderChildIndex()
{
	std::vector<std::vector<unsigned int>> elderChildOut;
	if (fromVTK)
	{
#ifdef USE_VTK
		auto tmpHTG = (vtkHyperTreeGrid *) HTG;

		unsigned long size;

		for (unsigned int i = 0; i < tmpHTG->GetMaxNumberOfTrees(); ++i)
		{
			auto ht = tmpHTG->GetTree(i);
			if (ht == nullptr)
			{
				elderChildOut.emplace_back();
				continue;
			}
			auto elderChild = ht->GetElderChildIndexArray(size);
			std::vector<unsigned int> elderChildVector(elderChild, elderChild + size);
			elderChildOut.push_back(elderChildVector);
		}
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		auto tmpHTG = (openHyperTreeGrid *) HTG;
		auto cursor = openHyperTreeGridCursorOriented(*tmpHTG);
		for (unsigned int i = 0; i < tmpHTG->getMaxNumberOfTrees(); ++i)
		{
			if (cursor.toTree(i))
			{
				elderChildOut.push_back(cursor.getHyperTree()->getElderChildIndex());
			}
			else
			{
				elderChildOut.emplace_back();
			}
		}
#endif
	}
	return elderChildOut;
}

void HTGDataGetter::getBounds(double boundsOut[6])
{
	if (fromVTK)
	{
#ifdef USE_VTK
		((vtkHyperTreeGrid *) HTG)->GetBounds(boundsOut);
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		((openHyperTreeGrid *) HTG)->getBounds(boundsOut);
#endif
	}
}

unsigned int HTGDataGetter::getMaxNumberOfTrees()
{
	if (fromVTK)
	{
#ifdef USE_VTK
		return ((vtkHyperTreeGrid *) HTG)->GetMaxNumberOfTrees();
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		return ((openHyperTreeGrid *) HTG)->getMaxNumberOfTrees();
#endif
	}
}

std::vector<std::string> HTGDataGetter::getScalarNames()
{

	if (fromVTK)
	{
#ifdef USE_VTK
		std::vector<std::string> scalarNames;
		auto cellData = ((vtkHyperTreeGrid *) HTG)->GetCellData();
		for (unsigned int i = 0; i < cellData->GetNumberOfArrays(); ++i)
		{
			scalarNames.emplace_back(cellData->GetArrayName(i));
		}
		return scalarNames;
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		return ((openHyperTreeGrid *) HTG)->getscalarArrayNames();
#endif
	}
}

void HTGDataGetter::getDimensions(unsigned int dim[3])
{
	if (fromVTK)
	{
#ifdef USE_VTK
		((vtkHyperTreeGrid *) HTG)->GetDimensions(dim);
		dim[0]--;
		dim[1]--;
		dim[2]--;
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		((openHyperTreeGrid *) HTG)->getDimensions(dim);
#endif
	}
}

std::vector<unsigned int> HTGDataGetter::getNumberofTreeNodes()
{
	std::vector<unsigned int> numberOfNodes;
	if (fromVTK)
	{
#ifdef USE_VTK
		auto tmpHTG = ((vtkHyperTreeGrid *) HTG);
		for (unsigned i = 0; i < tmpHTG->GetMaxNumberOfTrees(); ++i)
		{
			auto tree = tmpHTG->GetTree(i);
			if (tree)
			{
				numberOfNodes.push_back(tree->GetNumberOfVertices());
			}
			else
			{
				numberOfNodes.push_back(0);
			}
		}
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		auto tmpHTG = ((openHyperTreeGrid *) HTG);
		auto cursor = openHyperTreeGridCursorOriented(*tmpHTG);
		for (unsigned i = 0; i < tmpHTG->getMaxNumberOfTrees(); ++i)
		{
			if (cursor.toTree(i))
			{
				numberOfNodes.push_back(cursor.getHyperTree()->getNumberOfNodes());
			}
			else
			{
				numberOfNodes.push_back(0);
			}
		}
#endif
	}
	return numberOfNodes;
}

unsigned int HTGDataGetter::getNumberOfLevels()
{
	if (fromVTK)
	{
#ifdef USE_VTK
		return ((vtkHyperTreeGrid *) HTG)->GetNumberOfLevels();
#endif
	}
	else
	{
#ifdef USE_OPENHTG
		return ((openHyperTreeGrid *) HTG)->getNumberOfLevels();
#endif
	}
}