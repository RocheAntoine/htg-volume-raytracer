## HyperTree-Grid volume raytracer

Direct volume rendering with raytracing for Tree-based AMR data structure HyperTreeGrid

### Features :
- CPU and GPU rendering
- Interactive rendering (optional)
- Transfer function (on one primary and one secondary scalars)
- Level of Details (LOD) to improve rendering smoothness 
- Screenshot 
- Transfer function IO from disk
- Command line interface and scripting
- Graphical user interface
- Large data scale support

### Requirement :
- Cmake 
- GCC 
- Cuda (optional) 
- OpenHTG or VTK
- GLFW3 (needed if you want interactive rendering)

### Installation :

git clone git@gitlab.com:RocheAntoine/htg-volume-raytracer.git\


mkdir <build_dir>\
cd <build_dir>\
cmake <source_dir>\
ccmake <build_dir> (Config directories + options)\
make (-j)

### Usage :

<build_dir>/main
- -f [openHTG/VTK-HTG input file] 
- -d [scalar field to display] (optional)
- -s [script processing before interactive rendering] (optional if interactive rendering enable)

![YA31_rendering](https://gitlab.com/RocheAntoine/htg-volume-raytracer/-/wikis/uploads/391a70dab0957be16ab99e87fc390b2f/image__17_.png)
![YA31_rendering](https://gitlab.com/RocheAntoine/htg-volume-raytracer/-/wikis/uploads/f22cd3a77897e97a11fa692f8783cdbb/screen_snd_28k_bwr_8k__1_.jpg)
![GUI_example](https://gitlab.com/RocheAntoine/htg-volume-raytracer/-/wikis/uploads/1e965873b0a0c1805ce244664239a6c9/image__16_.png)
![All_YA31_timesteps](https://gitlab.com/RocheAntoine/htg-volume-raytracer/-/wikis/uploads/6ad6d13afe92f1ccee7254215135d8a0/475frames_60fps.mp4)
