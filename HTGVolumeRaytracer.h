//
// Created by antoine on 25/06/2020.
//

#ifndef HTGVOLUMERAYTRACER_HTGVOLUMERAYTRACER_H
#define HTGVOLUMERAYTRACER_HTGVOLUMERAYTRACER_H

#include <cstdint>

//class openHyperTreeGrid;
class HTGGPURaytracer;

class GLFWwindow;

class openHyperTreeGrid;

class HTGVolumeRaytracer
{
private:

	static void updateBuffer();

	static unsigned scalarToColor(float scalar);

	static void resetCamera();

	static void updateTransferFunction(bool primary);

#ifdef USE_INTERACTIVE_RENDERING_OPENGL

	static void glfwDisplay();

	static void windowResize(GLFWwindow *window, int w, int h);

	static void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);

	static void mouseMoveCallback(GLFWwindow *window, double x, double y);

	static void imguiWindow();

#endif

public:
	static void init();

	static void setHTG(void* htgIn, bool fromVTK, const char *fieldname = nullptr);

	static bool setScalarToShow(const char *scalarName);

	static bool setScalarOpacity(const char *scalarName);

	static void displayScalars();

	static bool setScalarOpacityMode(bool on);

	static void setScalarContributionMode(bool on);

	static void setMinMaxScalar(bool min, float value);

	static void resetMinMaxScalar(bool color);

	static void displayScalarInfo(bool color);

	static void saveBuffer(const char *filename, HTGGPURaytracer kernel);

	static void screenShotNewKernel(unsigned int width, unsigned int height, const char *filename);

	static void rotate(float x, float y);

	static void move(float x, float y);

	static void zoom(float l);

	static void displayCameraInfo();

	static void setCameraPosition(float x, float y, float z);

	static void setCameraLookAt(float x, float y, float z);

	static void transferFunctionSetMinMax(bool min, bool color, float value);
	static void transferFunctionRemoveMinMax(bool min, bool color);

	static void transferFunctionAddPoint(bool primary, float key, float value);

	static void transferFunctionRemovePoint(bool primary, float key);

	static void transferFunctionShow(bool primary);

	static void transferFunctionReset(bool primary);

	static void lookupTableUpdate();

	static void destroyAndExit();

#ifdef USE_INTERACTIVE_RENDERING_OPENGL

	static void renderHTG();

#endif
};


#endif //HTGVOLUMERAYTRACER_HTGVOLUMERAYTRACER_H
