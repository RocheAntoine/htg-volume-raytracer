//
// Created by antoine on 25/06/2020.
//

#include "HTGVolumeRaytracer.h"
#include "tracer/HTGGPURaytracer.h"
#include "utils/host_utils.h"
#include <iostream>
#include <algorithm>
//#include "openhtg/openHyperTreeGrid.h"


//#include "openhtg/openHyperTreeGridCursor.h"
#include <sstream>
#include <fstream>
#include "utils/toojpeg.h"
#include "utils/TransferFunctionWidget.h"
#include <chrono>
#include <unistd.h>
#include "utils/HTGDataGetter.h"

#ifdef USE_INTERACTIVE_RENDERING_OPENGL

#include "GLFW/glfw3.h"
#include <thread>
#include <mutex>
#include "utils/imgui/imgui_impl_glfw.h"
#include "utils/imgui/imgui_impl_opengl2.h"
#include "utils/imgui/imgui.h"


bool imguiWantCaptureMouse;               // Set when Dear ImGui will use mouse inputs, in this case do not dispatch them to your main game/application (either way, always pass on mouse inputs to imgui). (e.g. unclicked mouse is hovering over an imgui window, widget is active, mouse was clicked over an imgui window, etc.).
bool imguiWantCaptureKeyboard;


#endif

#ifdef OPENHTG_USE_CUDA

#include <thrust/device_vector.h>
#include <thrust/extrema.h>

#define OHTG_DATA_VECTOR_TYPE thrust::device_vector
#define VECTOR_GET_DATA() data().get()

#else
#define OHTG_DATA_VECTOR_TYPE std::vector
#define VECTOR_GET_DATA() data()
#endif

GLFWwindow *window;

HTGDataGetter dataGetter;

GPUHyperTreeGrid GPUHTG;
int32_t width;
int32_t height;
int32_t screenShotSize[2];
char screenShotFilename[128];
uint8_t *buffer;
uint32_t frame;
camera_t cam;
double mouse_prev_pos[2];
bool press_button = false;
bool moved = true;
bool modifiedHTG = true;
bool rendering = false;
std::mutex renderingMutex;

float dataRange[2];
float dataRangeOpacity[2];
uint32_t maxLevel;
float opacityFactor = 100.f;

uint32_t nbScreenShots = 0;
TransferFunctionWidget *primaryTransferFunctionWidget;
TransferFunctionWidget *secondaryTransferFunctionWidget;
std::ofstream imageFile;

std::vector<std::string> scalarList;
std::string scalarListString;

std::vector<float> lutIndex;
std::vector<vec3> lutValue;

int scalarColorID;
int scalarOpacityID = -1;

OHTG_DATA_VECTOR_TYPE<unsigned long int> hyperTreeOffset;
OHTG_DATA_VECTOR_TYPE<unsigned int> elderChildIndex;
OHTG_DATA_VECTOR_TYPE<unsigned int> lookUpTable;
OHTG_DATA_VECTOR_TYPE<unsigned char> bitMask;
OHTG_DATA_VECTOR_TYPE<unsigned int> numberofNodes;
OHTG_DATA_VECTOR_TYPE<unsigned long int> elderChildOffset;
OHTG_DATA_VECTOR_TYPE<float> scalarOpacity;
OHTG_DATA_VECTOR_TYPE<float> scalarColor;

OHTG_DATA_VECTOR_TYPE<float> primaryTransferFunctionIndex;
OHTG_DATA_VECTOR_TYPE<float> primaryTransferFunctionValue;

OHTG_DATA_VECTOR_TYPE<float> secondaryTransferFunctionIndex;
OHTG_DATA_VECTOR_TYPE<float> secondaryTransferFunctionValue;

std::vector<float> hostPrimaryTransferFunctionIndex;
std::vector<float> hostPrimaryTransferFunctionValue;
std::vector<float> hostSecondaryTransferFunctionIndex;
std::vector<float> hostSecondaryTransferFunctionValue;

static HTGGPURaytracer kernel{};

void HTGVolumeRaytracer::updateBuffer()
{
	if (renderingMutex.try_lock())
	{
#ifdef OPENHTG_USE_CUDA
		if (modifiedHTG)
		{
			cudaMemcpy(kernel.htg, &GPUHTG, sizeof(GPUHyperTreeGrid), cudaMemcpyHostToDevice);
		}
#endif
		modifiedHTG = false;
		++kernel.maxLevel;
		kernel.render();

#ifdef OPENHTG_USE_CUDA
		cudaDeviceSynchronize();
#endif
		kernel.getBuffer(buffer);

#ifdef USE_INTERACTIVE_RENDERING_OPENGL
		std::stringstream windowTitle;

		windowTitle << "HTG renderer - ";
		if (GPUHTG.volumeRendering)
		{
			windowTitle << "volume";
		}
		else
		{
			windowTitle << "surface";
		}
		windowTitle << " mode - " << kernel.maxLevel + 1 << " / " << maxLevel << " level";
		glfwSetWindowTitle(window, windowTitle.str().c_str());
		renderingMutex.unlock();
#endif
	}
	rendering = false;
}

#ifdef USE_INTERACTIVE_RENDERING_OPENGL

void HTGVolumeRaytracer::imguiWindow()
{
	ImGui::Begin("Config");
	ImGui::Text("Scalar");
	if (ImGui::Combo("Scalar field to display", &scalarColorID, scalarListString.c_str()))
	{
		setScalarToShow(scalarList[scalarColorID].c_str());
	}
	if (ImGui::Checkbox("Color scalar contribution", &(GPUHTG.scalarContribution)))
	{
		modifiedHTG = true;;
	}
	if (ImGui::Combo("Scalar field for opacity", &scalarOpacityID, scalarListString.c_str()))
	{
		setScalarOpacity(scalarList[scalarOpacityID].c_str());
	}
	if (scalarOpacityID != -1)
	{
		if (ImGui::Checkbox("Opacity scalar contribution", &(GPUHTG.secondScalarContribution)))
		{
			modifiedHTG = true;;
		}
	}
	if (ImGui::SliderFloat("Min scalar", &(GPUHTG.scalarColorMin), dataRange[0], dataRange[1]))
	{
		modifiedHTG = true;;
	}
	if (ImGui::IsItemHovered())
	{
		ImGui::SetTooltip("Ctrl + left click to set value");
	}
	if (ImGui::SliderFloat("Max scalar", &(GPUHTG.scalarColorMax), dataRange[0], dataRange[1]))
	{
		modifiedHTG = true;;
	}
	if (ImGui::IsItemHovered())
	{
		ImGui::SetTooltip("Ctrl + left click to set value");
	}

	ImGui::Text("Background color");
	if (ImGui::ColorEdit3("", (float *) (&kernel.background)))
	{
		moved = true;
	}


	ImGui::Text("Opacity");
	float opacity = GPUHTG.opacityFactor * 0.01f;
	if (ImGui::SliderFloat("Opacity factor", &(opacity), 0, 10))
	{
		GPUHTG.opacityFactor = opacity * 100.f;
		modifiedHTG = true;;
	}

	primaryTransferFunctionWidget->updateUI();
	if (primaryTransferFunctionWidget->tfnChanged)
	{
		lookupTableUpdate();
		updateTransferFunction(true);
	}

	if (GPUHTG.secondScalarContribution)
	{
		secondaryTransferFunctionWidget->updateUI();

		if (secondaryTransferFunctionWidget->tfnChanged)
		{
			updateTransferFunction(false);
		}
	}
	ImGui::End();

	ImGui::Begin("Screenshot");

	ImGui::Text("Dimensions :");
	ImGui::SameLine();
	ImGui::InputInt2("", screenShotSize);
	ImGui::Text("File name");
	ImGui::InputText("", screenShotFilename, 128);
	if (ImGui::Button("Take Screenshot"))
	{
		screenShotNewKernel(screenShotSize[0], screenShotSize[1], screenShotFilename);
	}
	ImGui::End();
}

void HTGVolumeRaytracer::glfwDisplay()
{
	if ((moved || modifiedHTG || kernel.maxLevel < maxLevel - 1) && !rendering)
	{
		rendering = true;
		if (moved || modifiedHTG)
		{
			kernel.maxLevel = 1u;
		}
		std::thread updateBufferThread(updateBuffer);
		updateBufferThread.detach();
		moved = false;
	}
	imguiWindow();
	ImGui::Render();

	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, width, height);
	glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());

	glfwSwapBuffers(window);
	++frame;

}

void HTGVolumeRaytracer::mouseMoveCallback(GLFWwindow *windowIn, double x, double y)
{
	if (imguiWantCaptureMouse)
	{
		return;
	}
	if (glfwGetMouseButton(windowIn, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
	{
		const float dx = static_cast<float>( x - mouse_prev_pos[0] ) /
		                 static_cast<float>( width );
		const float dy = static_cast<float>( y - mouse_prev_pos[1] ) /
		                 static_cast<float>( height );
		const float dmax = fabsf(dx) > fabs(dy) ? dx : dy;
		const float scale = fminf(dmax, 0.9f);
		zoom(scale);

		moved = true;
	}
	if (glfwGetMouseButton(windowIn, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		const float dx = static_cast<float>( x - mouse_prev_pos[0] ) /
		                 static_cast<float>( width );
		const float dy = static_cast<float>( y - mouse_prev_pos[1] ) /
		                 static_cast<float>( height );

		rotate(dx, dy);

		moved = true;
	}
	if (glfwGetMouseButton(windowIn, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS)
	{
		const float dx = static_cast<float>( x - mouse_prev_pos[0] ) /
		                 static_cast<float>( width );
		const float dy = static_cast<float>( y - mouse_prev_pos[1] ) /
		                 static_cast<float>( height );

		move(dx, dy);

		moved = true;

	}
	mouse_prev_pos[0] = x;
	mouse_prev_pos[1] = y;
}


void HTGVolumeRaytracer::keyCallback(GLFWwindow *windowIn, int32_t key, int32_t scancode, int32_t action, int32_t mods)
{
	if (imguiWantCaptureKeyboard)
	{
		return;
	}
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
			case (GLFW_KEY_ESCAPE):
				glfwSetWindowShouldClose(windowIn, GLFW_TRUE);
				break;
			case (GLFW_KEY_M):
				GPUHTG.volumeRendering = !GPUHTG.volumeRendering;
				moved = true;
				break;
			case (GLFW_KEY_R):
			{
				resetCamera();
				break;
			}
			case (GLFW_KEY_S):
			{
				time_t rawtime;
				struct tm *timeinfo;
				char strBuffer[80];

				time(&rawtime);
				timeinfo = localtime(&rawtime);

				strftime(strBuffer, sizeof(strBuffer), "%Y-%m-%d_%H:%M:%S", timeinfo);
				std::string filename = "image_";
				filename += strBuffer;
				filename += "_";
				filename += std::to_string(++nbScreenShots);
				filename += ".jpg";
				saveBuffer(filename.c_str(), kernel);
				break;
			}
			case (43): // +
			{
				opacityFactor *= 1.1f;
				GPUHTG.opacityFactor = opacityFactor;
				moved = true;
				break;
			}
			case (GLFW_KEY_MINUS): // -
			{
				opacityFactor /= 1.1f;
				GPUHTG.opacityFactor = opacityFactor;
				moved = true;
				break;
			}
			case (' '):
			{
				kernel.maxLevel = maxLevel - 2;
				break;
			}
			default:
			{
//				std::cout << key << std::endl;
				break;
			}
		}
	}
}

void HTGVolumeRaytracer::windowResize(GLFWwindow *windowIn, int32_t w, int32_t h)
{
	renderingMutex.lock();
	glfwGetFramebufferSize(windowIn, &width, &height);
	cam.ratio = (float) width / (float) height;
	calculateCameraVariables(cam);
	kernel.camera = cam;
	kernel.setWindowSize(width, height);
	buffer = (unsigned char *) malloc(width * height * sizeof(char) * 4);

//	glViewport(0, 0, width, height);
	modifiedHTG = true;
	renderingMutex.unlock();
}

void HTGVolumeRaytracer::renderHTG()
{
	glfwInit();
	window = glfwCreateWindow(width, height, "HTG Volume raytracer", nullptr, nullptr);

	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, keyCallback);
	glfwSetFramebufferSizeCallback(window, windowResize);
	glfwSetCursorPosCallback(window, mouseMoveCallback);
	glfwSwapInterval(1);

//	buffer = (uint8_t *) malloc(sizeof(uint8_t) * width * height * 3);

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &io = ImGui::GetIO();
	(void) io;
	io.MouseDoubleClickTime = 1.f;

	ImGui::StyleColorsDark();

	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL2_Init();

//	updateBuffer();
	buffer = (unsigned char *) malloc(width * height * sizeof(char) * 4);

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();


		ImGui_ImplOpenGL2_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		imguiWantCaptureMouse = io.WantCaptureMouse;
		imguiWantCaptureKeyboard = io.WantCaptureKeyboard;

		glfwDisplay();
	}

}

#endif

void HTGVolumeRaytracer::move(float x, float y)
{
	cam.eye += cam.V * y;
	cam.eye += cam.U * -x;

	cam.look_at += cam.V * y;
	cam.look_at += cam.U * -x;


	calculateCameraVariables(cam);
	kernel.camera = cam;
	moved = true;
}

void HTGVolumeRaytracer::rotate(float x, float y)
{
	float length = (cam.eye - cam.look_at).length();
	cam.eye += cam.V * y * 2.f;
	cam.eye += cam.U * -x * 2.f;

	vec3 camDir = (cam.eye - cam.look_at).normalize();
	cam.eye = cam.look_at + camDir * length;

	calculateCameraVariables(cam);
	kernel.camera = cam;
	moved = true;
}

void HTGVolumeRaytracer::zoom(float l)
{
	cam.eye = cam.eye + (cam.look_at - cam.eye) * l;

	calculateCameraVariables(cam);
	kernel.camera = cam;
	moved = true;
}


void HTGVolumeRaytracer::init()
{
	width = 801;
	height = 600;
	frame = 0;

	kernel = HTGGPURaytracer();
	kernel.setWindowSize(width, height);
#ifdef OPENHTG_USE_CUDA
	cudaMalloc(&(kernel.htg), sizeof(GPUHyperTreeGrid));
#else
	kernel.htg = (GPUHyperTreeGrid *) malloc(sizeof(GPUHyperTreeGrid));
#endif

//		addHT();
	cam.eye = vec3(0.5f, 0.5f, -0.5f);
	cam.look_at = vec3(0.5f);
	cam.up = vec3(0.f, 1.f, 0.f);
	cam.ratio = (float) width / (float) height;

	calculateCameraVariables(cam);

	kernel.camera = cam;

}

uint32_t HTGVolumeRaytracer::scalarToColor(float scalar)
{
	uint32_t result = (uint32_t) (std::max((0.5f - scalar) * 2.f, 0.f) * 255.f);
	result += (uint32_t) ((1 - std::abs(scalar - 0.5f) * 2.f) * 255.f) << 8u;
	result += (uint32_t) (std::max((scalar - 0.5f) * 2.f, 0.f) * 255.f) << 16u;
	return result;
}


void HTGVolumeRaytracer::setHTG(void *htgIn, bool fromVTK, const char *fieldname)
{
//	GPUHyperTreeGrid tmpHTG;

	dataGetter.setHTG(htgIn, fromVTK);

	scalarList = dataGetter.getScalarNames();
	std::stringstream ss;
	for (auto &scalarString : scalarList)
	{
		ss << scalarString << '\0';
	}
	ss >> scalarListString;

	uint32_t dim[3];
	dataGetter.getDimensions(dim);

	double bounds[6];
	dataGetter.getBounds(bounds);
	vec3 htgScale(bounds[1] - bounds[0], bounds[3] - bounds[2], bounds[5] - bounds[4]);
	GPUHTG.dimensions = vec3(dim[0], dim[1], dim[2]);
	GPUHTG.minBound = vec3(0.f);
	GPUHTG.maxBound = htgScale / vec3::max(htgScale);

	hyperTreeOffset.resize(dataGetter.getMaxNumberOfTrees());
	bitMask = dataGetter.getBitMask();
	numberofNodes.resize(dataGetter.getMaxNumberOfTrees());
	elderChildOffset.resize(dataGetter.getMaxNumberOfTrees());

	uint64_t elderOffset = 0;
	uint64_t scalarOffset = 0;
	GPUHTG.init();

	auto elderChildVector = dataGetter.getElderChildIndex();
	auto numberofNodesVector = dataGetter.getNumberofTreeNodes();
	int i = 0;
	for (auto &elderChild : elderChildVector)
	{
		if (!elderChild.empty())
		{
			hyperTreeOffset[i] = scalarOffset;
			scalarOffset += numberofNodesVector[i];
			numberofNodes[i] = elderChild.size();
			elderChildIndex.insert(elderChildIndex.end(), elderChild.begin(), elderChild.end());
			elderChildOffset[i] = elderOffset;
			elderOffset += elderChild.size();
		}
		else
		{
			numberofNodes[i] = 0;
		}
		++i;
	}


	setScalarToShow((fieldname && *fieldname != 0) ? fieldname : scalarList[0].c_str());

	GPUHTG.elderChildIndex = elderChildIndex.VECTOR_GET_DATA();
	GPUHTG.hyperTreeOffset = hyperTreeOffset.VECTOR_GET_DATA();

	GPUHTG.lookUpTable = lookUpTable.VECTOR_GET_DATA();

	if (!bitMask.empty())
	{
		GPUHTG.bitMask = bitMask.VECTOR_GET_DATA();
	}
	else
	{
		GPUHTG.bitMask = nullptr;
	}

	GPUHTG.numberofNodes = numberofNodes.VECTOR_GET_DATA();
	GPUHTG.elderChildOffset = elderChildOffset.VECTOR_GET_DATA();

	maxLevel = dataGetter.getNumberOfLevels();
	GPUHTG.opacityFactor = opacityFactor;

	GPUHTG.volumeRendering = true;
	GPUHTG.scalarContribution = true;
	GPUHTG.secondScalarContribution = false;


#ifndef OPENHTG_USE_CUDA
	kernel.htg = &GPUHTG;
#endif
}

void HTGVolumeRaytracer::resetCamera()
{
	cam.eye = vec3(0.5f, 0.5f, -0.5f);
	cam.look_at = vec3(0.5f);
	cam.up = vec3(0.f, 1.f, 0.f);
	cam.ratio = (float) width / (float) height;

	calculateCameraVariables(cam);
	kernel.camera = cam;
	moved = true;
}

bool HTGVolumeRaytracer::setScalarToShow(const char *scalarName)
{
	int tmpScalarID = -1, i = 0;
	for (auto &scalar : scalarList)
	{
		if (scalarName == scalar)
		{
			tmpScalarID = i;
			break;
		}
		++i;
	}
	if (tmpScalarID == -1)
	{
		return false;
	}
	scalarColorID = tmpScalarID;

	std::vector<float> tmpScalar;
	dataGetter.getScalar(scalarName, tmpScalar);
	scalarColor = std::move(tmpScalar);

#ifdef OPENHTG_USE_CUDA
	auto minmax = thrust::minmax_element(scalarColor.begin(), scalarColor.end());
	dataRange[0] = *(minmax.first);
	dataRange[1] = *(minmax.second);
#else
	auto minmax = std::minmax_element(scalarColor.begin(), scalarColor.end());
	dataRange[0] = *(minmax.first);
	dataRange[1] = *(minmax.second);
#endif
	transferFunctionReset(true);

	updateTransferFunction(true);

	lookupTableUpdate();

#ifdef OPENHTG_USE_CUDA
	GPUHTG.scalar = scalarColor.VECTOR_GET_DATA();
#else
	GPUHTG.scalar = scalarColor.data();
#endif
	GPUHTG.scalarColorMin = dataRange[0];
	GPUHTG.scalarColorMax = dataRange[1];

	modifiedHTG = true;;
	moved = true;
	return true;
}

bool HTGVolumeRaytracer::setScalarOpacity(const char *scalarName)
{
	int tmpScalarID = -1, i = 0;
	for (auto &scalar : scalarList)
	{
		if (scalarName == scalar)
		{
			tmpScalarID = i;
			break;
		}
		++i;
	}
	if (tmpScalarID == -1)
	{
		return false;
	}
	scalarOpacityID = tmpScalarID;

	std::vector<float> htgScalarOpacity;
	dataGetter.getScalar(scalarName, htgScalarOpacity);
	scalarOpacity = htgScalarOpacity;

#ifdef OPENHTG_USE_CUDA
	auto minmax = thrust::minmax_element(scalarOpacity.begin(), scalarOpacity.end());
	dataRangeOpacity[0] = *(minmax.first);
	dataRangeOpacity[1] = *(minmax.second);
#else
	auto minmax = std::minmax_element(scalarOpacity.begin(), scalarOpacity.end());
	dataRangeOpacity[0] = *(minmax.first);
	dataRangeOpacity[1] = *(minmax.second);
#endif

	transferFunctionReset(false);

	updateTransferFunction(false);

	GPUHTG.scalarOpacityMin = dataRangeOpacity[0];
	GPUHTG.scalarOpacityMax = dataRangeOpacity[1];
#ifdef OPENHTG_USE_CUDA
	GPUHTG.scalarOpacity = scalarOpacity.VECTOR_GET_DATA();
#else
	GPUHTG.scalarOpacity = scalarOpacity.data();
#endif

	GPUHTG.secondScalarContribution = true;

	modifiedHTG = true;;
	moved = true;
	return true;
}

void HTGVolumeRaytracer::displayScalars()
{
	std::cout << "Scalar list : " << std::endl << std::endl;
	for (auto &scalar : scalarList)
	{
		std::cout << scalar << std::endl;
	}
	std::cout << std::endl;
}

bool HTGVolumeRaytracer::setScalarOpacityMode(bool on)
{
	if (on)
	{
		if (scalarOpacity.empty())
		{
			return false;
		}
	}

	GPUHTG.secondScalarContribution = on;
	modifiedHTG = true;;
	moved = true;
	return true;
}

void HTGVolumeRaytracer::setScalarContributionMode(bool on)
{
	GPUHTG.scalarContribution = on;
	moved = true;
	modifiedHTG = true;;
}

void HTGVolumeRaytracer::setMinMaxScalar(bool min, float value)
{
	if (min)
	{
		GPUHTG.scalarColorMin = value;
	}

	else
	{
		GPUHTG.scalarColorMax = value;
	}

	modifiedHTG = true;;
	moved = true;
}

void HTGVolumeRaytracer::resetMinMaxScalar(bool color)
{
	if (color)
	{
		GPUHTG.scalarColorMin = dataRange[0];
		GPUHTG.scalarColorMax = dataRange[1];
	}
	else
	{
		GPUHTG.scalarOpacityMin = dataRangeOpacity[0];
		GPUHTG.scalarOpacityMax = dataRangeOpacity[1];
	}
	modifiedHTG = true;;
	moved = true;
}

void HTGVolumeRaytracer::displayScalarInfo(bool color)
{
	if (color)
	{
		std::cout << "Scalar name : " << scalarList[scalarColorID] << std::endl <<
		          "Min value : " << dataRange[0] << " ; Current Min : " << GPUHTG.scalarColorMin << std::endl <<
		          "Max value : " << dataRange[1] << " ; Current Max : " << GPUHTG.scalarColorMax << std::endl;
	}
	else
	{
		std::cout << "Scalar name : " << scalarList[scalarOpacityID] << std::endl <<
		          "Min value : " << dataRangeOpacity[0] << " ; Current Min : " << GPUHTG.scalarOpacityMin
		          << std::endl <<
		          "Max value : " << dataRangeOpacity[1] << " ; Current Max : " << GPUHTG.scalarOpacityMax
		          << std::endl;
	}
}

void byteOutput(uint8_t byte)
{
	imageFile << byte;
}

void HTGVolumeRaytracer::saveBuffer(const char *filename, HTGGPURaytracer kernelIn)
{
	auto tmpBuffer = (uint8_t *) malloc(sizeof(uint8_t) * kernelIn.height * kernelIn.width * 4);
	kernelIn.getBuffer(tmpBuffer);
	imageFile.open(filename, std::ofstream::out | std::ofstream::trunc | std::ofstream::binary);
	TooJpeg::writeJpeg(byteOutput, tmpBuffer, kernelIn.width, kernelIn.height, true, 100, true);
	imageFile.close();
	std::cout << "Image written in '" << filename << "'" << std::endl;
}

void HTGVolumeRaytracer::screenShotNewKernel(uint32_t widthIn, uint32_t heightIn, const char *filename)
{
	HTGGPURaytracer newKernel;

	newKernel.setWindowSize(widthIn, heightIn);

	newKernel.maxLevel = maxLevel;
	newKernel.background = kernel.background;
	newKernel.camera = kernel.camera;
	newKernel.htg = kernel.htg;
	newKernel.camera.ratio = (float) widthIn / (float) heightIn;

	calculateCameraVariables(newKernel.camera);
#ifdef OPENHTG_USE_CUDA
	if (modifiedHTG)
	{
		cudaMemcpy(kernel.htg, &GPUHTG, sizeof(GPUHyperTreeGrid), cudaMemcpyHostToDevice);
		modifiedHTG = false;
	}
#endif
	auto begin = std::chrono::high_resolution_clock::now();
	newKernel.render();
#ifdef OPENHTG_USE_CUDA
	cudaDeviceSynchronize();
#endif
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = end - begin;
	std::cout << "Frame rendered in " << elapsed.count() << " seconds" << std::endl;
	saveBuffer(filename, newKernel);
#ifdef OPENHTG_USE_CUDA
	cudaFree(newKernel.colorBuffer);
#else
	free(newKernel.colorBuffer);
#endif
}

void HTGVolumeRaytracer::displayCameraInfo()
{
	std::cout << "Camera info :\n"
	             "Position : " << cam.eye.x << " , " << cam.eye.y << " , " << cam.eye.z << std::endl <<
	          "Look at : " << cam.look_at.x << " , " << cam.look_at.y << " , " << cam.look_at.z << std::endl;
}

void HTGVolumeRaytracer::setCameraPosition(float x, float y, float z)
{
	cam.eye = vec3(x, y, z);
	kernel.camera = cam;
	moved = true;
}

void HTGVolumeRaytracer::setCameraLookAt(float x, float y, float z)
{
	cam.look_at = vec3(x, y, z);
	kernel.camera = cam;
	moved = true;
}

void HTGVolumeRaytracer::updateTransferFunction(bool primary)
{
	if (primary)
	{
		primaryTransferFunctionIndex = hostPrimaryTransferFunctionIndex;
		primaryTransferFunctionValue = hostPrimaryTransferFunctionValue;

		GPUHTG.primaryTransferFunctionIndex = primaryTransferFunctionIndex.VECTOR_GET_DATA();
		GPUHTG.primaryTransferFunctionValue = primaryTransferFunctionValue.VECTOR_GET_DATA();
	}
	else
	{
		secondaryTransferFunctionIndex = hostSecondaryTransferFunctionIndex;
		secondaryTransferFunctionValue = hostSecondaryTransferFunctionValue;

		GPUHTG.secondaryTransferFunctionIndex = secondaryTransferFunctionIndex.VECTOR_GET_DATA();
		GPUHTG.secondaryTransferFunctionValue = secondaryTransferFunctionValue.VECTOR_GET_DATA();
	}
//	transferFunctionShow(primary);
	modifiedHTG = true;;
	moved = true;
}

void HTGVolumeRaytracer::transferFunctionRemoveMinMax(bool min, bool color)
{
	if (min && color)
	{
		transferFunctionRemovePoint(color, dataRange[0]);
	}
	else if (!min && color)
	{
		transferFunctionRemovePoint(color, dataRange[1]);
	}

	else if (min)
	{
		transferFunctionRemovePoint(color, dataRangeOpacity[0]);
	}
	else
	{
		transferFunctionRemovePoint(color, dataRangeOpacity[1]);
	}
}

void HTGVolumeRaytracer::transferFunctionSetMinMax(bool min, bool color, float value)
{
	if (min && color)
	{
		hostPrimaryTransferFunctionValue.front() = value;
	}

	else if (!min && color)
	{
		hostPrimaryTransferFunctionValue.back() = value;
	}

	else if (min)
	{
		hostSecondaryTransferFunctionValue.front() = value;
	}

	else
	{
		hostSecondaryTransferFunctionValue.back() = value;
	}
	updateTransferFunction(color);
}

void HTGVolumeRaytracer::transferFunctionAddPoint(bool primary, float key, float value)
{
	if (primary)
	{
		uint32_t i;
		for (i = 0; i < hostPrimaryTransferFunctionIndex.size(); ++i)
		{
			if (key == hostPrimaryTransferFunctionIndex[i])
			{
				hostPrimaryTransferFunctionIndex.erase(hostPrimaryTransferFunctionIndex.begin() + i);
				hostPrimaryTransferFunctionValue.erase(hostPrimaryTransferFunctionValue.begin() + i);
				break;
			}
			if (key < hostPrimaryTransferFunctionIndex[i + 1])
			{
				break;
			}
		}
		hostPrimaryTransferFunctionIndex.insert(hostPrimaryTransferFunctionIndex.begin() + i, key);
		hostPrimaryTransferFunctionValue.insert(hostPrimaryTransferFunctionValue.begin() + i, value);
	}
	else
	{
		uint32_t i;
		for (i = 0; i < hostSecondaryTransferFunctionIndex.size(); ++i)
		{
			if (key == hostSecondaryTransferFunctionIndex[i])
			{
				hostSecondaryTransferFunctionIndex.erase(hostSecondaryTransferFunctionIndex.begin() + i);
				hostSecondaryTransferFunctionValue.erase(hostSecondaryTransferFunctionValue.begin() + i);
				break;
			}
			if (key < hostSecondaryTransferFunctionIndex[i + 1])
			{
				break;
			}
		}
		hostSecondaryTransferFunctionIndex.insert(hostSecondaryTransferFunctionIndex.begin() + i, key);
		hostSecondaryTransferFunctionValue.insert(hostSecondaryTransferFunctionValue.begin() + i, value);
	}
	updateTransferFunction(primary);
}

void HTGVolumeRaytracer::transferFunctionRemovePoint(bool primary, float key)
{
	if (primary)
	{
		uint32_t i;
		for (i = 0; i < hostPrimaryTransferFunctionIndex.size(); ++i)
		{
			if (key == hostPrimaryTransferFunctionIndex[i])
			{
				hostPrimaryTransferFunctionIndex.erase(hostPrimaryTransferFunctionIndex.begin() + i);
				hostPrimaryTransferFunctionValue.erase(hostPrimaryTransferFunctionValue.begin() + i);
				break;
			}
		}
	}
	else
	{
		uint32_t i;
		for (i = 0; i < hostSecondaryTransferFunctionIndex.size(); ++i)
		{
			if (key == hostSecondaryTransferFunctionIndex[i])
			{
				hostSecondaryTransferFunctionIndex.erase(hostSecondaryTransferFunctionIndex.begin() + i);
				hostSecondaryTransferFunctionValue.erase(hostSecondaryTransferFunctionValue.begin() + i);
				break;
			}
		}
	}
	updateTransferFunction(primary);
}

void HTGVolumeRaytracer::transferFunctionShow(bool primary)
{
	if (primary)
	{
		std::cout << "Primary transfer function : " << std::endl;
		for (uint32_t i = 0; i < primaryTransferFunctionValue.size(); ++i)
		{
			std::cout << primaryTransferFunctionIndex[i] << " -> " << primaryTransferFunctionValue[i] << std::endl;
		}
	}
	else
	{
		std::cout << "Secondary transfer function : " << std::endl;
		for (uint32_t i = 0; i < secondaryTransferFunctionValue.size(); ++i)
		{
			std::cout << secondaryTransferFunctionIndex[i] << " -> " << secondaryTransferFunctionValue[i] << std::endl;
		}
	}
}

void HTGVolumeRaytracer::transferFunctionReset(bool primary)
{
	if (primary)
	{
		lutIndex.clear();
		lutValue.clear();

		hostPrimaryTransferFunctionIndex.clear();
		hostPrimaryTransferFunctionValue.clear();

		hostPrimaryTransferFunctionIndex.push_back(dataRange[0]);
		hostPrimaryTransferFunctionIndex.push_back(dataRange[1]);

		hostPrimaryTransferFunctionValue.push_back(0.f);
		hostPrimaryTransferFunctionValue.push_back(1.f);

		delete primaryTransferFunctionWidget;
		primaryTransferFunctionWidget = new TransferFunctionWidget(
				hostPrimaryTransferFunctionIndex,
				hostPrimaryTransferFunctionValue,
				lutIndex,
				lutValue,
				dataRange[0],
				dataRange[1],
				true,
				"Primary transfer function");

	}
	else
	{
		hostSecondaryTransferFunctionIndex.clear();
		hostSecondaryTransferFunctionValue.clear();

		hostSecondaryTransferFunctionIndex.push_back(dataRangeOpacity[0]);
		hostSecondaryTransferFunctionIndex.push_back(dataRangeOpacity[1]);

		hostSecondaryTransferFunctionValue.push_back(0.f);
		hostSecondaryTransferFunctionValue.push_back(1.f);

		delete secondaryTransferFunctionWidget;
		secondaryTransferFunctionWidget = new TransferFunctionWidget(
				hostSecondaryTransferFunctionIndex,
				hostSecondaryTransferFunctionValue,
				lutIndex,
				lutValue,
				dataRangeOpacity[0],
				dataRangeOpacity[1],
				false,
				"Secondary transfer function");

	}
	updateTransferFunction(primary);
}

void HTGVolumeRaytracer::lookupTableUpdate()
{
	std::vector<vec3> lookupTableFloat;
	std::vector<uint32_t> lookupTableUint(numberOfColor);
	std::vector<float> tmp;

	primaryTransferFunctionWidget->getSampledColorsAndOpacities(numberOfColor, tmp, lookupTableFloat);

	for (uint32_t i = 0; i < numberOfColor; ++i)
	{
		uint32_t color = (uint32_t) (lookupTableFloat[i].z * 255.f);
		color |= (uint32_t) (lookupTableFloat[i].y * 255.f) << 8u;
		color |= (uint32_t) (lookupTableFloat[i].x * 255.f) << 16u;
		lookupTableUint[i] = color;
	}
	lookUpTable = lookupTableUint;
	GPUHTG.lookUpTable = lookUpTable.VECTOR_GET_DATA();

}

void HTGVolumeRaytracer::destroyAndExit()
{
#ifdef USE_INTERACTIVE_RENDERING_OPENGL
	glfwDestroyWindow(window);
	glfwTerminate();
#endif
#ifdef OPENHTG_USE_CUDA
	cudaFree(kernel.colorBuffer);
	cudaFree(kernel.htg);
#else
	free(kernel.colorBuffer);
	free(kernel.htg);
#endif
	hyperTreeOffset.clear();
	elderChildIndex.clear();
	lookUpTable.clear();
	bitMask.clear();
	numberofNodes.clear();
	elderChildOffset.clear();
	scalarOpacity.clear();
	scalarColor.clear();

	primaryTransferFunctionIndex.clear();
	primaryTransferFunctionValue.clear();

	secondaryTransferFunctionIndex.clear();
	secondaryTransferFunctionValue.clear();

	hostPrimaryTransferFunctionIndex.clear();
	hostPrimaryTransferFunctionValue.clear();

	hostSecondaryTransferFunctionIndex.clear();
	hostSecondaryTransferFunctionValue.clear();


	exit(EXIT_SUCCESS);
}